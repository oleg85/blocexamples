import 'package:flutter/material.dart';
import 'package:untitled/infinite_list_lib/posts/view/posts_page.dart';

class PostsApp extends MaterialApp {
  const PostsApp({super.key}) : super(home: const PostsPage());
}
