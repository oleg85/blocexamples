import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:untitled/counter_lib/counter_app.dart';
import 'package:untitled/counter_lib/counter_observer.dart';
import 'package:untitled/infinite_list_lib/posts_app.dart';
import 'package:untitled/infinite_list_lib/simple_bloc_observer.dart';
import 'package:untitled/login_lib/authentication_app.dart';
import 'package:untitled/login_lib/packages/authentication_repository/authentication_repository.dart';
import 'package:untitled/login_lib/packages/user_repository/user_repository.dart';
import 'package:untitled/timer_lib/timer_app.dart';

void main() {
  //counter
  Bloc.observer = CounterObserver();
  runApp(const CounterApp());

  //timer
  //runApp(const TimerApp());

  //infinite list
  // Bloc.observer = SimpleBlocObserver();
  // runApp(const PostsApp());

  //login
  // runApp(AuthenticationApp(
  //   authenticationRepository: AuthenticationRepository(),
  //   userRepository: UserRepository(),
  // ));
}
